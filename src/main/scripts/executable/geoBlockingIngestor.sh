#! /bin/bash
if [ $# -ne 2 ]
then
        echo "WRONG PARAMETERS"
        echo "USAGE: sh geoBlockingIngestor.sh <JOB_NAME> <TENANT_NAME>"
        exit
fi

BASE=`dirname $0`

if [ $# -ne 2 ]; then
        APPLICATION_TENANT="."
else
        APPLICATION_TENANT=$2
fi

CFG_CONF=$BASE/../config

if [ "$APPLICATION_TENANT" = "." ]; then
        echo "USING DEFAULT TENANT"
else
        if [ ! -d $CFG_CONF/$APPLICATION_TENANT ]; then
                echo "TENANT ${APPLICATION_TENANT} DOES NOT EXIST!!"
                exit 1
        fi
fi

CFG_CONF=$BASE/../config/$APPLICATION_TENANT
. ${CFG_CONF}/geoblocking.properties

#### SSL MANAGMENT ####
#if [[ -z "${MYSQL_IS_ENABLED_SSL}" ]]; then
	#echo "Sourcing variables from "${CFG_CONF}"/environment";
	#cat ${CFG_CONF}/environment;
	#dos2unix ${CFG_CONF}/environment;
	#source ${CFG_CONF}/environment;
	#export $(cut -d= -f1 ${CFG_CONF}/environment)
#fi


SSL=""
SSL_VAL=""
if [[ "${MYSQL_IS_ENABLED_SSL}" == "true" ]]; then
	SSL="--ssl-ca="
	SSL_VAL="${MYSQL_CERTIFICATE_PATH:-}"
	echo ${SSL}${SSL_VAL}
fi

#### END SSL MANAGMENT ####




##################################INPUT DIR#####################################
INPUT_DIR=${INPUT_DIR}
LOG_DIR=$BASE/../log/$APPLICATION_TENANT
cur_date=`date '+%Y%m%d_%H%M%S'`
LOG_FILE=${LOG_DIR}/${cur_date}geoblocking.log
IPV4_FILENAME=${IPV4_FILENAME}
IPV6_FILENAME=${IPV6_FILENAME}
WL_FILENAME=${WL_FILENAME}
BL_FILENAME=${BL_FILENAME}
IPDBCOM_FILENAME=${IPDBCOM_FILENAME} ##Added for ip-db.com
DONE_DIR=${DONE_DIR}
ERROR_DIR=${ERROR_DIR}
######################IP LIST REPOSITORY ##########################
IPREPOSITORY_HOSTNAME=${IPREPOSITORY_HOSTNAME}
IPREPOSITORY_USER=${IPREPOSITORY_USER}
IPREPOSITORY_FILE_PATH=${IPREPOSITORY_FILE_PATH}
IPREPOSITORY_BASE_FILE_NAME=${IPREPOSITORY_BASE_FILE_NAME}
IPREPOSITORY_SSH_ACCESS_REQUIRED=${IPREPOSITORY_SSH_ACCESS_REQUIRED}

######################SPLITTED_DIR-SUFFIX-LINE_NUMBER ##########################

SPLITTED_DIR=${WORKING_DIR}
SUFFIX_IPV4="split_ipv4_"
SUFFIX_WHITE="split_white_"
SUFFIX_BLACK="split_black_"
LINE_SPLIT=${SPLIT_FILE_NUMBER_ROWS}
SUFFIX_IPV4_SINGLE="split_ipv4_single"
SUFFIX_WHITE_SINGLE="split_white_single"
SUFFIX_BLACK_SINGLE="split_black_single"
SUFFIX_IPV6="split_ipv6_"
SUFFIX_IPV6_SINGLE="split_ipv6_single"
#DB_HOST_GEO_TMP=${db_host_geo_tmp_path}

#############################CLEAN SPLITTED_DIR#################################

rm -f $SPLITTED_DIR/split*
rm -f $SPLITTED_DIR/bgp*
rm -f $DONE_DIR/*
#############################COPY FILES FROM REMOTE SERVER#################################

if [ "${REPORTING_SSH_ACCESS_REQUIRED}" = "Y" ]; then
        scp -r "${IPREPOSITORY_USER}@${IPREPOSITORY_HOSTNAME}:${IPREPOSITORY_FILE_PATH}/*" ${INPUT_DIR}
fi

EXIT_CODE=${?}

if [ ${EXIT_CODE} -ne 0 ] ; then
       # echo "Program terminated with exit code: $EXIT_CODE. Error during process. See logfile for error information." >> ${LOG_FILE}
       # echo "$(date '+%Y-%m-%d %H:%M:%S') | ERROR | geoBlockingManager.sh | Error during execution of users extractor.The batch will be interrupted." >> ${LOG_FILE}
        exit 1
fi



COUNT_FILE=0

######################################################################################
################ TRANSFORM FILE FROM IP-DB.COM IN IPV4 FORMAT#########################
######################################################################################
if [ -f $INPUT_DIR/$IPDBCOM_FILENAME ]
then
        count_row=`cat $INPUT_DIR/$IPDBCOM_FILENAME  | wc -l`
        cat $INPUT_DIR/$IPDBCOM_FILENAME | grep -v ':' > $INPUT_DIR/dbip-country_ip.csv

        echo "Extracted only rows in IPV4 format"
        cat $INPUT_DIR/dbip-country_ip.csv | grep -E "${COUNTRY_INPUT_FILTER}" > $INPUT_DIR/dbip-country_IT_SM.csv
        echo "Extracted only rows for $COUNTRY_INPUT_FILTER"
        echo "Start processing"

        while read line
        do
                count=$((count+1));
                country=`echo ${line} | cut -d ',' -f3  $i`
                it="\"IT\""
                ch="\"CH\""
                va="\"VA\""
                sm="\"SM\""

                countryDescription="${COUNTRY_INPUT_FILTER}"

                primoIp=`echo ${line} | cut -d ',' -f1  $i`
                primoIpclear=`echo ${primoIp} | cut -d '"' -f2  $i`
                secondoIp=`echo ${line} | cut -d ',' -f2  $i`
                secondoIpclear=`echo ${secondoIp} | cut -d '"' -f2  $i`
                PART1=$((256*256*256*`echo ${primoIpclear} | cut -d '.' -f1  $i`))
                PART2=$((256*256*`echo ${primoIpclear} | cut -d '.' -f2  $i`))
                PART3=256*`echo ${primoIpclear} | cut -d '.' -f3  $i`
                PART4=`echo ${primoIpclear} | cut -d '.' -f4  $i`
                NUMERIC_FIRST_IP=$(($PART1+$PART2+$PART3+$PART4))
                PART1_2=$((256*256*256*`echo ${secondoIpclear} | cut -d '.' -f1  $i`))
                PART2_2=$((256*256*`echo ${secondoIpclear} | cut -d '.' -f2  $i`))
                PART3_2=256*`echo ${secondoIpclear} | cut -d '.' -f3  $i`
                PART4_2=`echo ${secondoIpclear} | cut -d '.' -f4  $i`
                NUMERIC_SECOND_IP=$(($PART1_2+$PART2_2+$PART3_2+$PART4_2))

                echo "\"${NUMERIC_FIRST_IP}\",\"${NUMERIC_SECOND_IP}\",$country,$country,\"\",\"\",0,0,\"\",\"\"" >> $INPUT_DIR/dbip-country_out.csv

                if [ $(($count % 50)) -eq 0 ]
                then
                        echo "Rows elaborated "$count;
                fi
        done < $INPUT_DIR/dbip-country_IT_SM.csv
        cat $INPUT_DIR/dbip-country_out.csv  >> $INPUT_DIR/$IPV4_FILENAME
else
		echo NO IP-DB.COM FILE IS PRESENT IN $INPUT_DIR/
       # echo NO IP-DB.COM FILE IS PRESENT IN $INPUT_DIR/ >> ${LOG_FILE}
fi
#cat $INPUT_DIR/dbip-country_out.csv  >> $INPUT_DIR/$IPV4_FILENAME

######################################################################################
################ END TRANSFORM FILE FROM IP-DB.COM IN IPV4 FORMAT#####################
######################################################################################


#########################CHECK IPV4 FILE###########################
if [ -f $INPUT_DIR/$IPV4_FILENAME ]
then

        ########### DO A BACKUP COPY OF THE IPV4 FILE #############
        cp ${INPUT_DIR}/${IPV4_FILENAME} ${INPUT_DIR}/_backup
		echo COPY COMPLETED ${INPUT_DIR}/${IPV4_FILENAME} TO ${INPUT_DIR}/_backup

        ### COUNTRY CODE FILTER FOR IPV4 FORMAT FILE 
        cat $INPUT_DIR/$IPV4_FILENAME | grep -E "${COUNTRY_INPUT_FILTER}" > $INPUT_DIR/IPV4_COUNTRY.csv
		mv $INPUT_DIR/IPV4_COUNTRY.csv $INPUT_DIR/$IPV4_FILENAME
        echo "Extracted only rows for $COUNTRY_INPUT_FILTER"
       # echo "Extracted only rows for $COUNTRY_INPUT_FILTER" >> ${LOG_FILE}

	####### COUNT SPLITTED LINE FOR IPV4 FILE ################

        COUNT_LINE_IPV4=`wc -l $INPUT_DIR/*ipv4* | cut -f 1 -d ' '`
		COUNT_LINE_SPLITTED_IPV4=0

		echo "NUMBER_OF_LINES IN ALL *IPV4* FILES - $COUNT_LINE_IPV4"
		
        ############### EXECUTE SPLIT COMMAND ####################
        if [ $COUNT_LINE_IPV4 -ne 0 ]
        then
		  cat $INPUT_DIR/*ipv4* | grep -i "\"" >> $INPUT_DIR/temp.log
				
		  if [ $LINE_SPLIT -gt 0 ]
		  then
				split -a 3 -l $LINE_SPLIT $INPUT_DIR/temp.log $SPLITTED_DIR/$SUFFIX_IPV4
				#cp $INPUT_DIR/temp.log $SPLITTED_DIR/$SUFFIX_IPV4_SINGLE
				rm -f $INPUT_DIR/temp.log
            else
				cp $INPUT_DIR/temp.log $SPLITTED_DIR/$SUFFIX_IPV4_SINGLE
				rm -f $INPUT_DIR/temp.log
		  fi	
	###VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR IPV4 FILE ###

                NUMBER_OF_SPLITTED_FILE_IPV4=`ls $SPLITTED_DIR/split_ipv4* | wc -l`
		  echo "Number of Splitted files: $NUMBER_OF_SPLITTED_FILE_IPV4"
				
                if [ $NUMBER_OF_SPLITTED_FILE_IPV4 -gt 1 ]
                then
                        COUNT_LINE_SPLITTED_IPV4=`wc -l $SPLITTED_DIR/split_ipv4* | grep total | cut -f 3 -d ' '`
                else
                        COUNT_LINE_SPLITTED_IPV4=`wc -l $SPLITTED_DIR/split_ipv4* | cut -f 1 -d ' '`
                fi
				
                ###END VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR IPV4 FILE ###
        else
                COUNT_LINE_SPLITTED_IPV4=0
        fi

        ###VERIFY SPLITTED LINE = ORIGINAL LINE FOR IPV4 FILE ###

		echo "NUMBER_OF_LINES IN ALL split_ipv4* FILES - $COUNT_LINE_SPLITTED_IPV4"
		
        if [ $COUNT_LINE_IPV4 -ne 0 ] && [ $COUNT_LINE_SPLITTED_IPV4 -ne 0 ] && [ $COUNT_LINE_IPV4 -eq $COUNT_LINE_SPLITTED_IPV4 ]
        then
                COUNT_FILE=$(($COUNT_FILE+1))
        fi
else
		echo NO IPV4 FILE IS PRESENT IN $INPUT_DIR/
        	#echo NO IPV4 FILE IS PRESENT IN $INPUT_DIR/ >> ${LOG_FILE}
fi

#########################END CHECK IPV4 FILE###########################




#########################CHECK WHITE LIST FILE###########################
if [ -f $INPUT_DIR/$WL_FILENAME ]
then

        ########### DO A BACKUP COPY OF THE WHITE FILE #############
        cp ${INPUT_DIR}/${WL_FILENAME} ${INPUT_DIR}/_backup_wl
		echo COPY COMPLETED ${INPUT_DIR}/${WL_FILENAME} TO ${INPUT_DIR}/_backup_wl
        
		    
	
        ####### COUNT SPLITTED LINE FOR WHITE LIST FILE ################

        COUNT_LINE_WL=`wc -l $INPUT_DIR/*white_list* | cut -f 1 -d ' '`
		COUNT_LINE_SPLITTED_WL=0

		echo "NUMBER_OF_LINES IN ALL *WL* FILES - $COUNT_LINE_WL"
		
        ############### EXECUTE SPLIT COMMAND ####################
        if [ $COUNT_LINE_WL -ne 0 ]
        then
				cat $INPUT_DIR/*white_list* | grep -i "\"" >> $INPUT_DIR/temp_wl.log
				
		  if [ $LINE_SPLIT -gt 0 ]
		  then
				split -a 3 -l $LINE_SPLIT $INPUT_DIR/temp_wl.log $SPLITTED_DIR/$SUFFIX_WHITE
				#cp $INPUT_DIR/temp.log $SPLITTED_DIR/$SUFFIX_WHITE_SINGLE
				rm -f $INPUT_DIR/temp_wl.log
            else
				cp $INPUT_DIR/temp_wl.log $SPLITTED_DIR/$SUFFIX_WHITE_SINGLE
				rm -f $INPUT_DIR/temp_wl.log
		  fi	
		  ###VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR WL FILE ###

                NUMBER_OF_SPLITTED_FILE_WL=`ls $SPLITTED_DIR/split_white* | wc -l`
		  echo "Number of Splitted files: $NUMBER_OF_SPLITTED_FILE_WL"
				
                if [ $NUMBER_OF_SPLITTED_FILE_WL -gt 1 ]
                then
                        COUNT_LINE_SPLITTED_WL=`wc -l $SPLITTED_DIR/split_white* | grep total | cut -f 3 -d ' '`
                else
                        COUNT_LINE_SPLITTED_WL=`wc -l $SPLITTED_DIR/split_white* | cut -f 1 -d ' '`
                fi
				
                ###END VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR WHITE LIST FILE ###
        else
                COUNT_LINE_SPLITTED_WL=0
        fi

        ###VERIFY SPLITTED LINE = ORIGINAL LINE FOR WHITE LIST FILE ###

		echo "NUMBER_OF_LINES IN ALL split_white* FILES - $COUNT_LINE_SPLITTED_WL"
		
        if [ $COUNT_LINE_WL -ne 0 ] && [ $COUNT_LINE_SPLITTED_WL -ne 0 ] && [ $COUNT_LINE_WL -eq $COUNT_LINE_SPLITTED_WL ]
        then
                COUNT_FILE=$(($COUNT_FILE+1))
        fi
else
		echo NO WHITE LIST FILE IS PRESENT IN $INPUT_DIR/
       # echo NO WHITE LIST FILE IS PRESENT IN $INPUT_DIR/ >> ${LOG_FILE}
fi

#########################END CHECK WHITE LIST FILE###########################




#########################CHECK BLACK LIST FILE###########################
if [ -f $INPUT_DIR/$BL_FILENAME ]
then

        ########### DO A BACKUP COPY OF THE BLACK FILE #############
        cp ${INPUT_DIR}/${BL_FILENAME} ${INPUT_DIR}/_backup_bl
		echo COPY COMPLETED ${INPUT_DIR}/${BL_FILENAME} TO ${INPUT_DIR}/_backup_bl
        
		    
	
        ####### COUNT SPLITTED LINE FOR BLACK LIST FILE ################

        COUNT_LINE_BL=`wc -l $INPUT_DIR/*black_list* | cut -f 1 -d ' '`
		COUNT_LINE_SPLITTED_BL=0

		echo "NUMBER_OF_LINES IN ALL *BL* FILES - $COUNT_LINE_BL"
		
        ############### EXECUTE SPLIT COMMAND ####################
        if [ $COUNT_LINE_BL -ne 0 ]
        then
				cat $INPUT_DIR/*black_list* | grep -i "\"" >> $INPUT_DIR/temp_bl.log
				
		  if [ $LINE_SPLIT -gt 0 ]
		  then
				split -a 3 -l $LINE_SPLIT $INPUT_DIR/temp_bl.log $SPLITTED_DIR/$SUFFIX_BLACK
				#cp $INPUT_DIR/temp.log $SPLITTED_DIR/$SUFFIX_BLACK_SINGLE
				rm -f $INPUT_DIR/temp_bl.log
            else
				cp $INPUT_DIR/temp_bl.log $SPLITTED_DIR/$SUFFIX_BLACK_SINGLE
				rm -f $INPUT_DIR/temp_bl.log
		  fi	
		  ###VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR BLACK LIST FILE ###

                NUMBER_OF_SPLITTED_FILE_BL=`ls $SPLITTED_DIR/split_black* | wc -l`
		  echo "Number of Splitted files: $NUMBER_OF_SPLITTED_FILE_BL"
				
                if [ $NUMBER_OF_SPLITTED_FILE_BL -gt 1 ]
                then
                        COUNT_LINE_SPLITTED_BL=`wc -l $SPLITTED_DIR/split_black* | grep total | cut -f 3 -d ' '`
                else
                        COUNT_LINE_SPLITTED_BL=`wc -l $SPLITTED_DIR/split_black* | cut -f 1 -d ' '`
                fi
				
                ###END VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR BLACK LIST FILE ###
        else
                COUNT_LINE_SPLITTED_BL=0
        fi

        ###VERIFY SPLITTED LINE = ORIGINAL LINE FOR BLACK FILE ###

		echo "NUMBER_OF_LINES IN ALL split_black* FILES - $COUNT_LINE_SPLITTED_BL"
		
        if [ $COUNT_LINE_BL -ne 0 ] && [ $COUNT_LINE_SPLITTED_BL -ne 0 ] && [ $COUNT_LINE_BL -eq $COUNT_LINE_SPLITTED_BL ]
        then
                COUNT_FILE=$(($COUNT_FILE+1))
        fi
else
		echo NO BLACK LIST FILE IS PRESENT IN $INPUT_DIR/
       # echo NO BLACK LIST FILE IS PRESENT IN $INPUT_DIR/ >> ${LOG_FILE}
fi

#########################END CHECK BLACK LIST FILE###########################












#########################CHECK IPV6 FILE###########################
if [ -f $INPUT_DIR/$IPV6_FILENAME ]
then

        ########### DO A BACKUP COPY OF THE IPV4 FILE #############
        cp ${INPUT_DIR}/${IPV6_FILENAME} ${INPUT_DIR}/_backupIpv6

        ### COUNTRY CODE FILTER FOR IPV6 FORMAT FILE 
        cat $INPUT_DIR/$IPV6_FILENAME | grep -E "${COUNTRY_INPUT_FILTER}" > $INPUT_DIR/IPV6_COUNTRY.csv
        echo "Extracted only rows for $COUNTRY_INPUT_FILTER"
		#echo "Extracted only rows for $COUNTRY_INPUT_FILTER" >> ${LOG_FILE}

        mv $INPUT_DIR/IPV6_COUNTRY.csv $INPUT_DIR/$IPV6_FILENAME
	
        ####### COUNT SPLITTED LINE FOR IPV6 FILE ################

        COUNT_LINE_IPV6=`wc -l $INPUT_DIR/*ipv6* | cut -f 1 -d ' '`
		COUNT_LINE_SPLITTED_IPV6=0

		echo "NUMBER_OF_LINES IN ALL *IPV6* FILES - $COUNT_LINE_IPV6"
		
        ############### EXECUTE SPLIT COMMAND ####################
        if [ $COUNT_LINE_IPV6 -ne 0 ]
        then
                cat $INPUT_DIR/*ipv6* | grep -i "\"" >> $INPUT_DIR/tempIpv6.log
				
		  if [ $LINE_SPLIT -gt 0 ]
		  then
		  	split -a 3 -l $LINE_SPLIT $INPUT_DIR/tempIpv6.log $SPLITTED_DIR/$SUFFIX_IPV6
			rm -f $INPUT_DIR/tempIpv6.log
                else
   		  	cp $INPUT_DIR/temp.log $SPLITTED_DIR/$SUFFIX_IPV6_SINGLE
			rm -f $INPUT_DIR/tempIpv6.log
    		  fi	
		  ###VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR IPV6 FILE ###

                NUMBER_OF_SPLITTED_FILE_IPV6=`ls $SPLITTED_DIR/split_ipv6* | wc -l`
		  echo "Number of Splitted Ipv6 files: $NUMBER_OF_SPLITTED_FILE_IPV6"
		
                if [ $NUMBER_OF_SPLITTED_FILE_IPV6 -gt 1 ]
                then
                        COUNT_LINE_SPLITTED_IPV6=`wc -l $SPLITTED_DIR/split_ipv6* | grep total | cut -f 3 -d ' '`
                else
                        COUNT_LINE_SPLITTED_IPV6=`wc -l $SPLITTED_DIR/split_ipv6* | cut -f 1 -d ' '`
                fi
                ###END VERIFY OUTPUT SPLITTED FILE_NUMBER AND COUNT LINE FOR IPV6 FILE ###
        else
                COUNT_LINE_SPLITTED_IPV6=0
        fi

        ###VERIFY SPLITTED LINE = ORIGINAL LINE FOR IPV6 FILE ###

        echo "NUMBER_OF_LINES IN ALL split_ipv6* FILES - $COUNT_LINE_SPLITTED_IPV6"
		if [ $COUNT_LINE_IPV6 -ne 0 ] && [ $COUNT_LINE_SPLITTED_IPV6 -ne 0 ] && [ $COUNT_LINE_IPV6 -eq $COUNT_LINE_SPLITTED_IPV6 ];
        then
                COUNT_FILE=$(($COUNT_FILE+1))
        fi
else
		echo NO IPV6 FILE IS PRESENT IN $INPUT_DIR/
       # echo NO IPV6 FILE IS PRESENT IN $INPUT_DIR/ >> ${LOG_FILE}
fi

#########################END CHECK IPV6 FILE###########################

###### sftp and db insert start######

if [ $COUNT_FILE -gt 0 ]
then

#echo -e "$(date '+%Y-%m-%d %H:%M:%S') | INFO | geoblockingIngestor | Start geoblocking process." >> ${LOG_FILE}
echo -e "$(date '+%Y-%m-%d %H:%M:%S') | INFO | geoblockingIngestor | Start geoblocking process."


#echo ""
#echo "**************************************************************************"
#echo "!!! Enter the passwrod when prompted for ${db_host_user}@${db_host_ip} !!!"
#echo "**************************************************************************"
#echo ""
COMMAND_RESULT=0

# sftp connection removed for containerization AVS-35650
#sftp ${db_host_user}@${db_host_ip} << REMOTECOMMANDS 
#rm $DB_HOST_GEO_TMP/*
#rmdir $DB_HOST_GEO_TMP
#mkdir $DB_HOST_GEO_TMP
#put $SPLITTED_DIR/* $DB_HOST_GEO_TMP/
#exit
#REMOTECOMMANDS

#echo "IP file upload successful"
#echo "IP file upload successful" >> ${LOG_FILE}

echo "Start GeoBlocking db import..."
#echo "Start GeoBlocking db import..." >> ${LOG_FILE}


echo "Starting import IPV4"
COUNT_LINE_IPV4_TMP=0
if [ -f $INPUT_DIR/$IPV4_FILENAME ]
then
COUNT_LINE_IPV4_TMP=`wc -l $INPUT_DIR/*ipv4* | cut -f 1 -d ' '`
if [ $COUNT_LINE_IPV4_TMP -gt 0 ]
then
mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e " CREATE TABLE  ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP AS SELECT * FROM ENTITLEMENT.GEO_BLOCKING_IP_RANGE WHERE 1=2; ALTER TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP ADD PRIMARY KEY (IP_FROM, IP_TO, IPV6_FROM, IPV6_TO); "
COMMAND_RESULT=$(($COMMAND_RESULT + $?))
for file in $SPLITTED_DIR/$SUFFIX_IPV4*
do
	filename=$(basename $file)
	echo "importing Ipv4 into DB for filename:"$filename
	#echo "importing Ipv4 into DB for filename:"$filename >> ${LOG_FILE}
	mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "LOAD DATA LOCAL INFILE '$SPLITTED_DIR/$filename' REPLACE INTO TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' (ip_from, ip_to, country_code, country_name, region, city, latitude, longitude, zip_code) SET isp_name=null, domain_name=null, is_enabled='Y', is_managed='N', creation_date=now(), update_date=now();"
	COMMAND_RESULT=$(($COMMAND_RESULT + $?))
done
mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "RENAME TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP1, ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE; create index idx1 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE(IP_FROM) ; create index idx2 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE(IP_TO) ; DROP TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP1;"

COMMAND_RESULT=$(($COMMAND_RESULT + $?))
else
echo "Number of Lines to process in $IPV4_FILENAME are Zero(0)"
fi
fi






if [ -f $INPUT_DIR/$WL_FILENAME ]
then
COUNT_LINE_WL_TMP=`wc -l $INPUT_DIR/*white_list* | cut -f 1 -d ' '`
if [ $COUNT_LINE_WL_TMP -gt 0 ]
then
mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e " CREATE TABLE  ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST_TMP AS SELECT * FROM ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST WHERE 1=2; ALTER TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST_TMP ADD PRIMARY KEY (IP_FROM, IP_TO, IPV6_FROM, IPV6_TO); "
COMMAND_RESULT=$(($COMMAND_RESULT + $?))

for file in $SPLITTED_DIR/$SUFFIX_WHITE*
do
	filename=$(basename $file)
	echo "importing white list into DB for filename:"$filename
	#echo "importing white list into DB for filename:"$filename >> ${LOG_FILE}
	mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "LOAD DATA LOCAL INFILE '$SPLITTED_DIR/$filename' REPLACE INTO TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST_TMP FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' (ip_from, ip_to, country_code, country_name, region, city, latitude, longitude, zip_code) SET isp_name=null, domain_name=null, is_enabled='Y', is_managed='N', creation_date=now(), update_date=now();"
	COMMAND_RESULT=$(($COMMAND_RESULT + $?))
done

mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "RENAME TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST_TMP1, ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST_TMP TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST; create index idx1 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST(IP_FROM) ; create index idx2 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST(IP_TO) ; DROP TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_WHITE_LIST_TMP1;"

COMMAND_RESULT=$(($COMMAND_RESULT + $?))
else
echo "Number of Lines to process in $WL_FILENAME are Zero(0)"
fi
fi




if [ -f $INPUT_DIR/$BL_FILENAME ]
then
COUNT_LINE_BL_TMP=`wc -l $INPUT_DIR/*black_list* | cut -f 1 -d ' '`
if [ $COUNT_LINE_BL_TMP -gt 0 ]
then
mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e " CREATE TABLE  ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST_TMP AS SELECT * FROM ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST WHERE 1=2; ALTER TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST_TMP ADD PRIMARY KEY (IP_FROM, IP_TO, IPV6_FROM, IPV6_TO); "
COMMAND_RESULT=$(($COMMAND_RESULT + $?))

for file in $SPLITTED_DIR/$SUFFIX_BLACK*
do
	filename=$(basename $file)
	echo "importing black list into DB for filename:"$filename
	#echo "importing black list into DB for filename:"$filename >> ${LOG_FILE}
	mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "LOAD DATA LOCAL INFILE '$SPLITTED_DIR/$filename' REPLACE INTO TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST_TMP FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' (ip_from, ip_to, country_code, country_name, region, city, latitude, longitude, zip_code) SET isp_name=null, domain_name=null, is_enabled='Y', is_managed='N', creation_date=now(), update_date=now();"
	COMMAND_RESULT=$(($COMMAND_RESULT + $?))
done
mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "RENAME TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST_TMP1, ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST_TMP TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST; create index idx1 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST(IP_FROM) ; create index idx2 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST(IP_TO) ; DROP TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_BLACK_LIST_TMP1;"

COMMAND_RESULT=$(($COMMAND_RESULT + $?))
else
echo "Number of Lines to process in $BL_FILENAME are Zero(0)"
fi
fi


echo "Starting import IPV6"
COUNT_LINE_IPV6_TMP=0
if [ -f $INPUT_DIR/$IPV6_FILENAME ]
then
COUNT_LINE_IPV6_TMP=`wc -l $INPUT_DIR/*ipv6* | cut -f 1 -d ' '`

if [ $COUNT_LINE_IPV6_TMP -gt 0 ]
then

if [ $COUNT_LINE_IPV4_TMP -gt 0 ]
then
echo "Appending IPV6 to existing IPV4 configuration"
mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e " CREATE TABLE  ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP AS SELECT * FROM ENTITLEMENT.GEO_BLOCKING_IP_RANGE WHERE 1=1; ALTER TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP ADD PRIMARY KEY (IP_FROM, IP_TO, IPV6_FROM, IPV6_TO); "
COMMAND_RESULT=$(($COMMAND_RESULT + $?))
else
echo "Creanting IPV6 configuration"
mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e " CREATE TABLE  ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP AS SELECT * FROM ENTITLEMENT.GEO_BLOCKING_IP_RANGE WHERE 1=2; ALTER TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP ADD PRIMARY KEY (IP_FROM, IP_TO, IPV6_FROM, IPV6_TO); "
COMMAND_RESULT=$(($COMMAND_RESULT + $?))
fi

for fileIpv6 in $SPLITTED_DIR/$SUFFIX_IPV6*
do
	filenameIpv6=$(basename $fileIpv6)
	#echo "importing Ipv6 into DB for filename:"$filenameIpv6 >> ${LOG_FILE}
	mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "LOAD DATA LOCAL INFILE '$SPLITTED_DIR/$filenameIpv6' INTO TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' (ipv6_from, ipv6_to, country_code, country_name, region, city, latitude, longitude, zip_code) SET isp_name=null, domain_name=null, is_enabled='Y', is_managed='N', creation_date=now(), update_date=now();"
	COMMAND_RESULT=$(($COMMAND_RESULT + $?))
done

mysql ${SSL}${SSL_VAL} --user=${username} --password=${psw} --host=${db_host_ip} --port=3306 -e "RENAME TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP1, ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP TO ENTITLEMENT.GEO_BLOCKING_IP_RANGE; create index idx1 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE(IP_FROM) ; create index idx2 on ENTITLEMENT.GEO_BLOCKING_IP_RANGE(IP_TO) ; DROP TABLE ENTITLEMENT.GEO_BLOCKING_IP_RANGE_TMP1;"

COMMAND_RESULT=$(($COMMAND_RESULT + $?))
else 
echo "Number of Lines to process in $IPV6_FILENAME are Zero(0)"
fi
fi



###### Done and Error folder logic start######
 		if [ $COMMAND_RESULT -gt 0 ] # something went wrong.
                  then
				  if [ -f $INPUT_DIR/$IPV4_FILENAME ]
					then
                        #echo "Moving IPV4 from $INPUT_DIR/$IPV4_FILENAME to $ERROR_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving IPV4 from $INPUT_DIR/$IPV4_FILENAME to $ERROR_DIR/ Folder."
						mv $INPUT_DIR/$IPV4_FILENAME $ERROR_DIR/
				 fi
				  if [ -f $INPUT_DIR/$IPV6_FILENAME ]
					then
                        #echo "Moving IPV6 from $INPUT_DIR/$IPV6_FILENAME to $ERROR_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving IPV6 from $INPUT_DIR/$IPV6_FILENAME to $ERROR_DIR/ Folder."
						mv $INPUT_DIR/$IPV6_FILENAME $ERROR_DIR/
				  fi
				  if [ -f $INPUT_DIR/$WL_FILENAME ]
					then
                       # echo "Moving WHITE LIST from $INPUT_DIR/$WL_FILENAME to $ERROR_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving WHITE LIST from $INPUT_DIR/$WL_FILENAME to $ERROR_DIR/ Folder."
						mv $INPUT_DIR/$WL_FILENAME $ERROR_DIR/
				  fi
					if [ -f $INPUT_DIR/$BL_FILENAME ]
					then
                      #  echo "Moving BLACK LIST from $INPUT_DIR/$BL_FILENAME to $ERROR_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving BLACK LIST from $INPUT_DIR/$BL_FILENAME to $ERROR_DIR/ Folder."
						mv $INPUT_DIR/$BL_FILENAME $ERROR_DIR/
				  fi
                else
					if [ -f $INPUT_DIR/$IPV4_FILENAME ]
					then
						#echo "Moving IPV4 from $INPUT_DIR/$IPV4_FILENAME to $DONE_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving IPV4 from $INPUT_DIR/$IPV4_FILENAME to $DONE_DIR/ Folder."
						mv $INPUT_DIR/$IPV4_FILENAME $DONE_DIR/
					fi	
					if [ -f $INPUT_DIR/$IPV6_FILENAME ]
					then
						#echo "Moving IPV6 from $INPUT_DIR/$IPV6_FILENAME to $DONE_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving IPV6 from $INPUT_DIR/$IPV6_FILENAME to $DONE_DIR/ Folder."
						mv $INPUT_DIR/$IPV6_FILENAME $DONE_DIR/
					fi	
					if [ -f $INPUT_DIR/$WL_FILENAME ]
					then
						#echo "Moving WHITE LIST from $INPUT_DIR/$WL_FILENAME to $DONE_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving WHITE LIST from $INPUT_DIR/$WL_FILENAME to $DONE_DIR/ Folder."
						mv $INPUT_DIR/$WL_FILENAME $DONE_DIR/
					fi
					if [ -f $INPUT_DIR/$BL_FILENAME ]
					then
						#echo "Moving BLACK LIST from $INPUT_DIR/$BL_FILENAME to $DONE_DIR/ Folder." >> ${LOG_FILE}
						echo "Moving BLACK LIST from $INPUT_DIR/$BL_FILENAME to $DONE_DIR/ Folder."
						mv $INPUT_DIR/$BL_FILENAME $DONE_DIR/
					fi
                  fi
###### Done and Error folder logic end######
echo -e "$(date '+%Y-%m-%d %H:%M:%S') | INFO | geoblockingIngestor | End geoblocking process."
#echo -e "$(date '+%Y-%m-%d %H:%M:%S') | INFO | geoblockingIngestor | End geoblocking process." >> ${LOG_FILE}

###### sftp and db insert end######

        if [ "${RERPORTING_SSH_ACCESS_REQUIRED}" = "Y" ]; then
                ssh ${IPREPOSITORY_USER}@${IPREPOSITORY_HOSTNAME}  "rm -rf ${IPREPOSITORY_FILE_PATH}/*;exit;"
        fi

        ######## IF BACKUP EXISTS -> REMOVE THE DIRTY IPV4 AND RESTORE THE BACKUP ############
        if [ -f ${INPUT_DIR}/_backup ]
        then
                   ##rm ${INPUT_DIR}/${IPV4_FILENAME}
				   ##mv ${INPUT_DIR}/_backup ${INPUT_DIR}/${IPV4_FILENAME}
					rm ${INPUT_DIR}/_backup
					
				  if [ -f $INPUT_DIR/$IPDBCOM_FILENAME ]
				  then
                	rm ${INPUT_DIR}/${IPDBCOM_FILENAME}
				  fi
        fi
		
		if [ -f ${INPUT_DIR}/_backupIpv6 ]
			then
				##rm ${INPUT_DIR}/${IPV6_FILENAME}
				##mv ${INPUT_DIR}/_backupIpv6 ${INPUT_DIR}/${IPV6_FILENAME}
				rm ${INPUT_DIR}/_backupIpv6
		fi
		
		if [ -f ${INPUT_DIR}/_backup_wl ]
			then
				##rm ${INPUT_DIR}/${WL_FILENAME }
				##mv ${INPUT_DIR}/_backup_wl ${INPUT_DIR}/${WL_FILENAME}
				rm ${INPUT_DIR}/_backup_wl
		fi
		
		if [ -f ${INPUT_DIR}/_backup_bl ]
			then
				##rm ${INPUT_DIR}/${BL_FILENAME }
				##mv ${INPUT_DIR}/_backup_bl ${INPUT_DIR}/${BL_FILENAME}
				rm ${INPUT_DIR}/_backup_bl
		fi

else
        echo "Number of Lines to process are Zero(0) OR The count of lines in input file and the count of lines in the splitted files is not same!!"
fi

exit;
