#!/bin/bash
PATH=".."

CFG_CONF=/product/batches/GeoBlocking/config
. ${CFG_CONF}/geoblocking.properties
username=$1
psw=$2
db_ip=$3
port=$4

SSL=""
SSL_VAL=""
if [[ "${MYSQL_IS_ENABLED_SSL}" == "true" ]]; then
	SSL="--ssl-ca="
	SSL_VAL="${MYSQL_CERTIFICATE_PATH:-}"
	echo ${SSL}${SSL_VAL}
fi

/usr/bin/mysql ${SSL}${SSL_VAL} -u${username} -p${psw} -h${db_ip} -P${port} -Be "update avs_be.sys_parameters set param_value = 'N' where param_name = 'GEO_BLOCKING_IN_PANIC';"

if [ ! -z "$USE_HTTPS" ] && [ $USE_HTTPS == "true" ]; then
	echo "HTTPS is enabled."
	/usr/bin/curl -o $PATH/log/${db_ip}_ATT_GEO.txt --cacert $B2B_CA_FILENAME $MS_URL_SECURE -k
else 
	/usr/bin/curl -o $PATH/log/${db_ip}_ATT_GEO.txt $MS_URL
fi

#### PRINT OUTPUT PANIC DEACTIVATION --> GEOBLOCKING

echo UPDATE RESULT DB: `/usr/bin/mysql ${SSL}${SSL_VAL} -u${username} -p${psw} -h${db_ip} -P${port} -Be "select param_value from avs_be.sys_parameters where param_name = 'GEO_BLOCKING_IN_PANIC';"`

echo ################## WARNING IF UPDATE RESULT ON DB = N PANIC DB NOT ACTIVE

echo RESULT ReloadMapDBAction ON SERVER $db_ip: `/bin/cat $PATH/log/${db_ip}_ATT_GEO.txt`

#echo ################## WARNING IF RESULT ReloadMapDBAction = KO CONFIGURATION NOT RELOADED FROM AVS CORE
